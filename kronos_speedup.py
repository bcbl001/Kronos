###################################################################
### Ben Langley
### Kronos stock selection algorithm
### For more details please read kronos.txt
###################################################################
from linkedlist import LinkedList
from node import Node
from pandas_datareader import data
from math import ceil
from math import isnan

import pandas as pd
import make_tickers
import time as t
import csvoutput
import sys
import datetime

start_program_time = t.time()

# Define the variables needed
NEG_INF = -10000000
#NOT_A_NUMBER = float('nan')
NOT_A_NUMBER = None
score = LinkedList()
csvfile = 'StockNamesClean.csv'
tickers = []
time = 10.0
timeInterval = 1.0
outputfile = 'output/kronos_output.csv'
data_source = 'yahoo'
avgValues = 5
firstYearThreshold = 0.1
bulkLoad = 100

# Get date info
today = datetime.datetime.now()
tenYears = datetime.timedelta(days=365*10)
dateFormat = "%Y-%m-%d"

# Get the list of stocks to test
tickers = make_tickers.getStocks(csvfile)
#tickers = tickers[0:200]
#tickers = ['AAPL', 'MSFT', 'SSC', 'ZYME']
#tickers = ['AAPL', 'SSC', 'ZYME']

# Calculate number of intervals
numIntervals = int(time / timeInterval)

# Loop through each bulk load
numLoads = int(ceil(len(tickers) / float(bulkLoad)))

intervalStart = 0
for loadNum in range(1, numLoads + 1):
    # Find new interval end
    intervalEnd = intervalStart + bulkLoad

    # Check the interval end does not exceed bounds of tickers list
    if intervalEnd > len(tickers):
        intervalEnd = len(tickers)

    print "Stocks in interval", loadNum,"range: [",intervalStart,",",intervalEnd,")"

    # Load the correct data
    tickersToLoad = tickers[intervalStart : intervalEnd]

    # Get all data from past 10 years
    start_date = (today - tenYears).strftime(dateFormat)
    end_date = today.strftime(dateFormat)

    start_time = t.time()
    # User pandas_reader.data.DataReader to load the desired data. As simple as that.
    panel_data = data.DataReader(tickersToLoad, data_source, start_date, end_date)
    print "Read time: ", t.time() - start_time

    # Getting just the adjusted closing prices. This will return a Pandas DataFrame
    # The index in this DataFrame is the major index of the panel_data.
    adj_close = panel_data.ix['Adj Close']

    # Get order of stocks
    titles = list(adj_close.columns.values)

    # Iterate over each stock to calculate desired metrics
    for stock in titles:
        # Update loop variables
        print "Analyzing ", stock

        avgChangeArr = []
        oneYrAvgChange = 0
        oneYrValue = 1
        fiveYrAvgChange = []
        fiveYrValue = 1
        years_of_data = 0
        weight = 1.0
        weightedChangePercent = 0
        #isProfitable = True
        containsNaN = False

        # Check which order the data is coming in
        timeStamp = adj_close.index[0]
        datetimeStamp = timeStamp.to_pydatetime()
        startYear = datetimeStamp.year
        endYear = adj_close.index[len(adj_close.index) - 1].to_pydatetime().year
        if startYear == 2017:
            hasNewestFirst = True
        else:
            hasNewestFirst = False

        print "    start year: ", startYear
        print "    end year: ", endYear

        # Get the values of the stock as a list
        stockDataDF = adj_close.get(stock)
        stockData = stockDataDF.get_values() # Convert the DataFrame to a list

        # Define a year to be 1/numIntervals of the total length
        daysAYear = len(stockData) / numIntervals

        # Iterate over each time interval
        for year in range(0, numIntervals):
            # |
            # |
            # |
            # |
            # |
            # |
            # |
            # |
            #---------------------------
            #    start              end
            # Calculate the points needed and loop over 5 times
            if hasNewestFirst == True:
                # 2017 data is in the first index --> 2007 in the last index
                endIndex = daysAYear * year
                startIndex = daysAYear * (year + 1)
                start = stockData[startIndex : startIndex + 5]
                end = stockData[endIndex : endIndex + 5]
            else:
                # 2007 data is in the first index --> 2017 in the last index
                endIndex = len(stockData) - 1 - (daysAYear * year)
                startIndex = len(stockData) - 1 - (daysAYear * (year + 1))
                start = stockData[startIndex - 5 : startIndex]
                end = stockData[endIndex - 5 : endIndex]

            # Check if the start data contains NaN
            for dataValue in start:
                if isnan(dataValue):
                    print "    Not enough data. Years of data:", years_of_data
                    containsNaN = True
                    break
            if containsNaN:
                break

            # Check if the end data contains NaN
            for dataValue in end:
                if isnan(dataValue):
                    print "    Not enough data. Years of data:", years_of_data
                    containsNaN = True
                    break
            if containsNaN:
                break

            # Loop through 5 selection intervals
            avgChange = 0
            for i in range(0, avgValues):
                # Calculate percentage change using midpoint method
                midpoint = (end[i] + start[i]) / 2
                difference = end[i] - start[i]
                change = difference / midpoint
                avgChange += change

            # Take the average of all 5 lines
            avgChange = avgChange / avgValues
            avgChangeArr.append(avgChange)

            '''
            # This was needed before data speedup
            # Check previous year from today is positive
            if (year == 1) and (avgChange < firstYearThreshold):
                print "Not profitable!"
                isProfitable = False
                break
            '''

            # 5 year calculation
            if year <= 4:
                fiveYrAvgChange.append(avgChange)

            # Increment Years of Data
            years_of_data = year + 1

        '''
        # If stock is not profitable do not consider it
        if isProfitable == False:
            continue
        '''

        # Check if we have 1 yr data
        if years_of_data >= 1:
            # Set one yr value
            oneYrAvgChange = avgChangeArr[0]
            oneYrValue += oneYrAvgChange

            # Calculate the weighted percent change
            weightedChangePercent = 0
            for avgChange in avgChangeArr:
                weightedChangePercent += weight * avgChange
                weight -= timeInterval / time
            weightedChangePercent = weightedChangePercent / years_of_data
        else:
            # Set place holder values for 1yr value and weighted change percent
            oneYrValue = float('NaN')
            weightedChangePercent = NEG_INF

        # Check if we have 5 yr data
        if years_of_data >= 5:
            # Calculate 5 yr value
            for yrAvgChange in fiveYrAvgChange:
                fiveYrValue = fiveYrValue + (fiveYrValue * yrAvgChange)
        else:
            fiveYrValue = float('NaN')

        # Add to the linked list
        node = Node(stock, weightedChangePercent, oneYrValue, fiveYrValue, startYear, endYear, years_of_data)
        score.add(node)
        end_loop_time = t.time()

    # Update the start index for the next interval
    intervalStart = intervalEnd

# Write the output to csvfile
header = ['Stock Symbol', 'Score', '1 yr Value', '5 yr Value', 'Yr of Data', 'Start Yr', 'End Yr']
data = [score.getStockList(), score.getWeightedPercentChangeList(), score.getOneYearValueList(), score.getFiveYearValueList(), score.getYearsOfDataList(), score.getStartYearList(), score.getEndYearList()]
csvoutput.writeToCSV(outputfile, header, data)

end_program_time = t.time()
print "Entire calculation time: ", end_program_time - start_program_time

