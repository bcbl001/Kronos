##############################################
### Ben Langley
### Linked List NODE implementation
###     This file defines all necessary
###     methods for a node in linked list
###
### To import and use
###     import node
###     node = Node()
###     node.get_data()
##############################################
class Node:

    ### Initializes a new node for a linked list
    def __init__(self, stockSymbol, weightedPercentChange, oneYearValue, fiveYearValue, startYear, endYear, yearsOfData):
        ### Instance variables for the node
        self.stockSymbol = stockSymbol # The stock symbol for this node
        self.weightedPercentChange = weightedPercentChange # The weighted percent change for this stock node
        self.oneYearValue = oneYearValue # The value of the stock in 1 year
        self.fiveYearValue = fiveYearValue # The value of the stock in 5 years
        self.startYear = startYear # The start year of the data set
        self.endYear = endYear # The end year of the data set
        self.yearsOfData = yearsOfData # The years of valid data
        self.nextNode = None # The next node in the linked list
        self.prevNode = None # The previous node in the linked list

    ### Gets the weighted percent change for this node
    ### @return the node weighted percent change
    def getValue(self):
        return self.weightedPercentChange

    ### Gets the stock symbol for this node
    ### @return the node stock symbol
    def getSymbol(self):
        return self.stockSymbol

    ### Gets the stock value in 1 year
    ### @return the predicted value in a year
    def getOneYearValue(self):
        return self.oneYearValue

    ### Gets the stock value in 5 years
    ### @return the predicted value in 5 years
    def getFiveYearValue(self):
        return self.fiveYearValue

    ### Gets the start year of the data set
    ### @return the integer start year
    def getStartYear(self):
        return self.startYear

    ### Gets the end year of the data set
    ### @return the integer end year
    def getEndYear(self):
        return self.endYear

    ### Gets the years of data in the set
    ### @param the number of years of data
    def getYearsOfData(self):
        return self.yearsOfData

    ### Gets the next node in the linked list
    ### @return the next node
    def getNextNode(self):
        return self.nextNode

    ### Gets the previous node in the linked list
    ### @return the previous node
    def getPrevNode(self):
        return self.prevNode

    ### Sets the predicted one year value
    ### @param the new predicted value
    def setOneYearValue(self, oneYearValue):
        self.oneYearValue = oneYearValue

    ### Sets the predicted 5 year value
    ### @param the new predicted value
    def setFiveYearValue(self, fiveYearValue):
        self.fiveYearValue = fiveYearValue

    ### Sets the start year of the data set
    ### @param the new start year
    def setStartYear(self, startYear):
        self.startYear = startYear

    ### Sets the end year of the data set
    ### @param the new end year
    def setEndYear(self, endYear):
        self.endYear = endYear

    ### Sets the years of data in the set
    ### @param the number of valid yeard
    def setYearsOfData(self, yearsOfData):
        self.yearsOfData = yearsOfData

    ### Sets the next node
    ### @param node the new next node
    def setNextNode(self, node):
        self.nextNode = node

    ### Sets the previous node
    ### @param node the new previous node
    def setPrevNode(self, node):
        self.prevNode = node

