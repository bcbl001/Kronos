############################
### Ben Langley
### Write an array into the columns of the csv file
###
### To check functionality uncomment bottom and run
###     python csvoutput.py
###
### To use in project import with
###     import csvoutput
#############################
import csv
import sys

def writeToCSV(filename, fieldnames, cols):
    # Check if number of columns equal between fieldnames and cols
    numCols = len(cols) # Number of columns
    if len(fieldnames) != numCols:
        print "Length of fieldnames(", len(fieldnames), ") does not equal number of columns(", numCols, ")"
        sys.exit(1)

    # Check if each column is the same size
    lenCol = len(cols[0])
    for col in cols:
        if len(col) != lenCol:
            print "Length of columns are not the same!"
            sys.exit(-1)

    # Open the file for writing
    with open(filename, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # Print header row
        writer.writerow(fieldnames)

        # Print each row at a time
        for i in range(0,lenCol):
            # Create a list to represent the row
            row = []
            for j in range(0,numCols):
                row.append(cols[j][i])
            writer.writerow(row)

'''
###########
## DEBUG ##
###########
names = ['BEN', 'SYD', 'RAN', 'NEW']
values = [1, 2, 3, 4]
values2 = [1, 4, 9, 16]
values3 = [2, 6, 12, 20]
fieldnames = ['name', 'value', 'value^2', 'value + value^2']
cols = [names, values, values2, values3]

writeToCSV('output/output.csv', fieldnames, cols)
'''
