##############################################
### Ben Langley
### Linked List ADT implementation
###     This file defines all necessary
###     methods for single linked list
###
### To import and use
###     import linkedlist
###     list = LinkedList()
###     list.add(..,..)
###     list.sort(...,...)
###     list.get(...)
##############################################
import node

class LinkedList:

    ### Initializes a new instance of linked list ADT
    def __init__(self):
        ### Instance variables for the linked list
        self.front = None # The front of the linked list
        self.back = None # The back of the linked list
        self.count = 0 # The number of items in the linked list

    ### Adds a new node to the correct place of the ordered list
    def add(self, node):
        if self.count == 0:
            self.front = node
            self.back = node
            self.count += 1
        else:
            # Find the correct place in the list to add it
            # Begin with the currentNode being the front
            currentNode = self.front
            # Loop through each node until we find the place to be
            while node.getValue() < currentNode.getValue():
                # Update currentNode if next is not None
                if currentNode.getNextNode() is not None:
                    currentNode = currentNode.getNextNode()
                else:
                    ## Add to the end of the list
                    node.setNextNode(None)
                    node.setPrevNode(currentNode)
                    currentNode.setNextNode(node)
                    self.back = node
                    self.count += 1
                    return

            # The variable currentNode holds the largest node less than node
            # Update the surrounding nodes appropriately
                # currentNode.prevNode -- node -- currentNode
            if currentNode.getPrevNode() is None:
                ## Add to the front of the list
                node.setNextNode(currentNode)
                node.setPrevNode(None) # Should be None already, but just to be sure
                currentNode.setPrevNode(node)
                self.front = node
            else:
                nodePrev = currentNode.getPrevNode()
                node.setPrevNode(nodePrev)
                currentNode.setPrevNode(node)
                node.setNextNode(currentNode)
                nodePrev.setNextNode(node)

            # Increment count variable
            self.count += 1

    ### Gets the number of nodes in the linked list
    ### @return the number of nodes in the linked list
    def getCount(self):
        return self.count

    ### Gets the linked list as a list of ordered stock symbols
    ### @return a list of the stock symbols ordered by weighted percent change
    def getStockList(self):
        stockList = []
        currentNode = self.front
        while currentNode is not None:
            stockList.append(currentNode.getSymbol())
            currentNode = currentNode.getNextNode()
        return stockList

    ### Gets the linked list as a list of ordered score values
    ### @return a list of the score values (weighted percent change)
    def getWeightedPercentChangeList(self):
        scoreList = []
        currentNode = self.front
        while currentNode is not None:
            scoreList.append(currentNode.getValue())
            currentNode = currentNode.getNextNode()
        return scoreList

    ### Gets the linked list as a list of one year values
    ### @return a list of the one year values
    def getOneYearValueList(self):
        valueList = []
        currentNode = self.front
        while currentNode is not None:
            valueList.append(currentNode.getOneYearValue())
            currentNode = currentNode.getNextNode()
        return valueList

    ### Gets the linked list as a list of five year values
    ### @return a list of the five year values
    def getFiveYearValueList(self):
        valueList = []
        currentNode = self.front
        while currentNode is not None:
            valueList.append(currentNode.getFiveYearValue())
            currentNode = currentNode.getNextNode()
        return valueList

    ## Gets the linked list as a list of start years
    ### @return a list of the start years
    def getStartYearList(self):
        valueList = []
        currentNode = self.front
        while currentNode is not None:
            valueList.append(currentNode.getStartYear())
            currentNode = currentNode.getNextNode()
        return valueList

    ## Gets the linked list as a list of end years
    ### @return a list of the end years
    def getEndYearList(self):
        valueList = []
        currentNode = self.front
        while currentNode is not None:
            valueList.append(currentNode.getEndYear())
            currentNode = currentNode.getNextNode()
        return valueList

    ## Gets the linked list as a list of years of data
    ### @return a list of the years of data
    def getYearsOfDataList(self):
        valueList = []
        currentNode = self.front
        while currentNode is not None:
            valueList.append(currentNode.getYearsOfData())
            currentNode = currentNode.getNextNode()
        return valueList




