############################
### Ben Langley
### This program demonsrates how to read command line arguments
###
### To run use
###     python sysargv.py
#############################
import sys

print "This is the name of the script: ", sys.argv[0]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)
