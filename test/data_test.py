############################
### Ben Langley
### Python Stock data test
###
### To run use
###     python data_test.py
#############################
from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
import time as t
import make_tickers

# Define the instruments to download. We would like to see Apple, Microsoft and the S&P500 index.
tickers = ['AAPL', 'MSFT', '^GSPC', 'VNET']
tickers = make_tickers.getStocks('StockNamesClean.csv')
tickers = tickers[0:200]

# Define which online source one should use
data_source = 'yahoo'

# We would like all available data from 01/01/2000 until 12/31/2016.
start_date = '2000-01-01'
end_date = '2016-12-31'

start_time = t.time()
# User pandas_reader.data.DataReader to load the desired data. As simple as that.
panel_data = data.DataReader(tickers, data_source, start_date, end_date)
print "Read time: ", t.time() - start_time

# Getting just the adjusted closing prices. This will return a Pandas DataFrame
# The index in this DataFrame is the major index of the panel_data.
adj_close = panel_data.ix['Adj Close']

# Getting all weekdays between 01/01/2000 and 12/31/2016
all_weekdays = pd.date_range(start=start_date, end=end_date, freq='B')

# How do we align the existing prices in adj_close with our new set of dates?
# All we need to do is reindex adj_close using all_weekdays as the new index
adj_close = adj_close.reindex(all_weekdays)

# Reindexing will insert missing values (NaN) for the dates that were not present
# in the original set. To cope with this, we can fill the missing by replacing them
# with the latest available price for each instrument.
adj_close = adj_close.fillna(method='ffill')

# Print the top 7 elements for each item in tickers
#print adj_close.head(7)
print adj_close.tail(7)

'''
from pandas_datareaders_unofficial import DataReader
import datetime
import time

start_time = time()
expire_after = 60*60 # seconds - 0: no cache - None: no cache expiration

symbol = ["GOOG", "AAPL", "MSFT"]
end_date = datetime.datetime.now()
num_days = 200
start_date = end_date - datetime.timedelta(days=num_days)
data = DataReader("GoogleFinanceDaily", expire_after=expire_after).get(symbol, start_date, end_date)
print(data)
print "Total time: ", time() - start_time
'''
