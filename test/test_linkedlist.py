##############################################
### Ben Langley
### A test file for the funcionality of our linked list ADT
###
### To run use
###    python test_linkedlist.py
##############################################
from linkedlist import LinkedList
from node import Node

# Create a linked list
linkedList = LinkedList()

# Create 3 new nodes
node1 = Node("BEN", 2, 4, 5)
node2 = Node("SYD", 3, 4, 5)
node3 = Node("RAN", 4, 4, 5)
node4 = Node("NEW", 1, 4, 5)

# Add the nodes and print out the list each time
linkedList.add(node1)
print linkedList.getStockList()

linkedList.add(node3)
print linkedList.getStockList()

linkedList.add(node2)
print linkedList.getStockList()

linkedList.add(node4)
print linkedList.getStockList()
