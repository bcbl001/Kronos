############################
### Ben Langley
### Read in data from csv file and clean it up
###
### To run use:
###     python clean_input.py [INPUT FILENAME] [OUTPUT FILENAME]
#############################
from pandas_datareader import data
import pandas as pd
import csv
import sys

if len(sys.argv) < 3:
    print "Not enough arguments! Correct usage:"
    print "   python clean_input.py [INPUT FILE NAME] [OUTPUT FILE NAME]"
    sys.exit(1)

csvfile = sys.argv[1]
outputfile = sys.argv[2]
goodStocks = []
badStocks = []

# Define which online source one should use
data_source = 'yahoo'

# Open the file for reading
with open(csvfile, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for stockSymbol in reader:
        tickers = stockSymbol

        # For each stock check if we can read it
        start_date = "2017-9-07"
        end_date = "2017-10-07"

        print "Analyzing stock ", stockSymbol
        try:
            # User pandas_reader.data.DataReader to load the desired data. As simple as that.
            panel_data = data.DataReader(tickers, data_source, start_date, end_date)
            goodStocks.append(stockSymbol)
        except:
            badStocks.append(stockSymbol)
            continue

print "Good stocks: ", goodStocks
print "Bad stocks: ", badStocks

# Open the file for writing
with open(outputfile, 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

    # Print each row at a time
    for stockSymbol in goodStocks:
        # Create a list to represent the row
        writer.writerow(stockSymbol)
