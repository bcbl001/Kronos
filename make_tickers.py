############################
### Ben Langley
### Read in data from csv file and store it into an array
###
### To check functionality uncomment bottom and run
###     python make_tickers.py
###
### To use in project import with
###     import make_tickers
#############################
import csv

def getStocks(csvfile):
    with open(csvfile, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        stocksArr = []
        for stockSymbol in reader:
            stocksArr.append(stockSymbol[0])
        return stocksArr

'''
csvfile = 'StockNames.csv'
stocksArr = getStocks(csvfile)
count = 0
for stockSymbol in stocksArr:
    if count < 10:
        print stockSymbol
    count += 1
print "Length of stocksArr: ", len(stocksArr)
'''

