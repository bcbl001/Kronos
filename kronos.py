###################################################################
### Ben Langley
### Kronos stock selection algorithm
### For more details please read kronos.txt
###################################################################
from linkedlist import LinkedList
from node import Node
from pandas_datareader import data

import pandas as pd
import make_tickers
import time as t
import csvoutput
import sys
import datetime

start_program_time = t.time()

# Define the variables needed
score = LinkedList()
csvfile = 'StockNamesClean.csv'
tickers = []
time = 10.0
timeInterval = 1.0
outputfile = 'output/kronos_output.csv'
data_source = 'yahoo'
avgValues = 5
firstYearThreshold = 0.1

# Get date info
today = datetime.datetime.now()
tenDays = datetime.timedelta(days=10)
oneYear = datetime.timedelta(days=365)
dateFormat = "%Y-%m-%d"

# Get the list of stocks to test
tickers = make_tickers.getStocks(csvfile)
tickers = tickers[0:10]
#tickers = ['F', 'MSFT', 'AAPL']

# Calculate number of intervals
numInterval = int(time / timeInterval)

# Loop through each stock in the list
for stock in tickers:
    print "Analyzing ", stock
    start_loop_time = t.time()
    stockArr = [stock]
    avgChangeArr = []
    oneYrAvgChange = 0
    oneYrValue = 1
    fiveYrAvgChange = []
    fiveYrValue = 1
    weight = 1.0
    weightedChangePercent = 0
    endDate = today
    prevTail = None
    isProfitable = True

    # Loop through each time interval
    # Previous tail is current head
    for interval in range (1, numInterval + 1):
        # Head Calculation
        #====================================================================================
        # Check if head has been calculated before
        if interval == 1: # Head has not been calculated yet
            # Set the start and end date in the format YYYY-MM-DD
            startDate = endDate - tenDays
            start_date = startDate.strftime(dateFormat)
            end_date = endDate.strftime(dateFormat)

            # User pandas_reader.data.DataReader to load the desired data. As simple as that.
            panel_data = data.DataReader(stockArr, data_source, start_date, end_date)

            # Getting just the adjusted closing prices. This will return a Pandas DataFrame
            # The index in this DataFrame is the major index of the panel_data.
            adj_close = panel_data.ix['Adj Close']

            head = adj_close.head(avgValues).get_values() # First 5 closing values in list
        else: # Use previous tail as the head
            head = prevTail

        # Tail Calculations
        #====================================================================================
        # Set the start and end date in the format YYYY-MM-DD
        startDate = startDate - oneYear
        endDate = endDate - oneYear
        start_date = startDate.strftime(dateFormat)
        end_date = endDate.strftime(dateFormat)

        try:
            # User pandas_reader.data.DataReader to load the desired data. As simple as that.
            panel_data = data.DataReader(stockArr, data_source, start_date, end_date)
        except:
            print "Cannot find symbol or not enough data"
            break

        # Getting just the adjusted closing prices. This will return a Pandas DataFrame
        # The index in this DataFrame is the major index of the panel_data.
        adj_close = panel_data.ix['Adj Close']

        tail = adj_close.head(avgValues).get_values() # Last 5 closing values in list

        # Update prevTail
        prevTail = tail

        #====================================================================================

        # Loop through 5 selection intervals
        avgChange = 0
        for i in range(0, avgValues):
            # Calculate percentage change (midpoint method?)
            difference = head[i] - tail[i]
            change = difference / tail[i]
            avgChange += change

        # Take the average of all 5 lines
        avgChange = avgChange / avgValues
        avgChangeArr.append(avgChange[0])

        # Check previous year from today is positive
        if (interval == 1) and (avgChange[0] < firstYearThreshold):
            print "Not profitable!"
            isProfitable = False
            break

        # 5 year calculation
        if interval <= 5:
            fiveYrAvgChange.append(avgChange[0])

        # Increment Years of Data
        years_of_data = interval

    # If stock is not profitable do not consider it
    if isProfitable == False:
        continue

    # Calculate one year and five year values
    oneYrAvgChange = avgChangeArr[0]
    oneYrValue += oneYrAvgChange
    for yrAvgChange in fiveYrAvgChange:
        fiveYrValue = fiveYrValue + (fiveYrValue * yrAvgChange)

    # Calculate the weighted percent change
    print avgChangeArr
    weightedChangePercent = 0
    #weight = 1.0
    for avgChange in avgChangeArr:
        weightedChangePercent += weight * avgChange
        weight -= timeInterval / time
    weightedChangePercent = weightedChangePercent / years_of_data

    # Add to the linked list
    node = Node(stock, weightedChangePercent, oneYrValue, fiveYrValue)
    score.add(node)
    end_loop_time = t.time()
    print "Stock calculation time: ", end_loop_time - start_loop_time

# Write the output to csvfile
header = ['Stock Symbol', 'Score', '1 yr Value', '5 yr Value']
data = [score.getStockList(), score.getWeightedPercentChangeList(), score.getOneYearValueList(), score.getFiveYearValueList()]
csvoutput.writeToCSV(outputfile, header, data)
end_program_time = t.time()
print "Entire calculation time: ", end_program_time - start_program_time



